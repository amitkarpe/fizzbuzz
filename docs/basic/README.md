
## Objective:
Using your favorite orchestration (i.e. Kubernetes) tools and platform, provide a working infrastructure-as-code example that provisions a service to execute an implementation of the FizzBuzz coding challenge with documentation on how to use and execute.

### Prerequisite:

- Working cluster with Master & Node
- Node should able to run all kubectl related command. ( copy /etc/kubernetes/admin.conf from Master to ~/.kube/config on Node1)

```console
cloud_user@amitkarpe2c:~$ kubectl get nodes
NAME                          STATUS   ROLES    AGE    VERSION
amitkarpe2c.mylabserver.com   Ready    master   109m   v1.13.4
amitkarpe3c.mylabserver.com   Ready    <none>   103m   v1.13.4

cloud_user@amitkarpe3c:~$ kubectl get nodes
NAME                          STATUS   ROLES    AGE    VERSION
amitkarpe2c.mylabserver.com   Ready    master   109m   v1.13.4
amitkarpe3c.mylabserver.com   Ready    <none>   103m   v1.13.4

```

- Get code from [Gitlab](https://gitlab.com/amitkarpe/fizzbuzz.git)
```console
git clone https://gitlab.com/amitkarpe/fizzbuzz.git
```

### Steps to run the code

```shell
git clone https://gitlab.com/amitkarpe/fizzbuzz.git
cd fizzbuzz/minimal/
make
```

### Troubleshooting

- Make sure all files include Dockerfile is present in the folder
```console
cloud_user@amitkarpe3c:~/fizzbuzz/minimal$ ls
Dockerfile  fizzbuzz.py  Makefile  output.txt  pod.yaml
```

- Make sure user have permission to build images

```console
cloud_user@amitkarpe3c:~/fizzbuzz/minimal$ docker info
Containers: 12
 Running: 4
 Paused: 0
 Stopped: 8
Images: 11
Server Version: 18.09.7
Storage Driver: aufs
 Root Dir: /var/lib/docker/aufs
 Backing Filesystem: extfs
 Dirs: 43
 Dirperm1 Supported: true
Logging Driver: json-file
Cgroup Driver: cgroupfs
Plugins:
 Volume: local
 Network: bridge host macvlan null overlay
 Log: awslogs fluentd gcplogs gelf journald json-file local logentries splunk syslog
Swarm: inactive
Runtimes: runc
Default Runtime: runc
Init Binary: docker-init
containerd version: 
runc version: N/A
init version: v0.18.0 (expected: fec3683b971d9c3ef73f284f176672c44b448662)
Security Options:
 apparmor
 seccomp
  Profile: default
Kernel Version: 4.4.0-1090-aws
Operating System: Ubuntu 16.04.6 LTS
OSType: linux
Architecture: x86_64
CPUs: 2
Total Memory: 3.801GiB
Name: amitkarpe3c.mylabserver.com
ID: Y7WR:Z5PE:WBTI:5JRU:HHEH:ACHW:XVFN:3DHG:SQPM:KPQI:BFWF:KKUJ
Docker Root Dir: /var/lib/docker
Debug Mode (client): false
Debug Mode (server): false
Registry: https://index.docker.io/v1/
Labels:
Experimental: false
Insecure Registries:
 127.0.0.0/8
Live Restore Enabled: false

WARNING: No swap limit support
```

- Make sure node1 have `fizzbuzz` docker image 
```console
cloud_user@amitkarpe3c:~/fizzbuzz/minimal$ docker images | grep fizz
fizzbuzz                 latest              b6b2201023c7        6 minutes ago       137MB
```

- Make sure user have permission to create k8s objects like pod.

```console
cloud_user@amitkarpe3c:~/fizzbuzz/minimal$ kubectl config view
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: DATA+OMITTED
    server: https://172.31.26.202:6443
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: kubernetes-admin
  name: kubernetes-admin@kubernetes
current-context: kubernetes-admin@kubernetes
kind: Config
preferences: {}
users:
- name: kubernetes-admin
  user:
    client-certificate-data: REDACTED
    client-key-data: REDACTED
```

- Review `kubectl describe pod fizzbuzz`
```console
cloud_user@amitkarpe3c:~/fizzbuzz/minimal$ kubectl describe pod fizzbuzz
Name:         fizzbuzz
Namespace:    default
Priority:     0
Node:         amitkarpe3c.mylabserver.com/172.31.24.57
Start Time:   Mon, 07 Oct 2019 05:52:44 +0000
Labels:       run=fizzbuzz
Annotations:  <none>
Status:       Succeeded
IP:           10.244.1.29
Containers:
  fizzbuzz:
    Container ID:   docker://427d94439ff19761bad7990021e7434c85e3175cf84f7d1b188ab0e3c5dce40c
    Image:          fizzbuzz
    Image ID:       docker://sha256:b6b2201023c7b552d251fcde873211a10110b225c418d31a5bdd9ac3d0b36c1b
    Port:           <none>
    Host Port:      <none>
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Mon, 07 Oct 2019 05:52:45 +0000
      Finished:     Mon, 07 Oct 2019 05:52:45 +0000
    Ready:          False
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-8mksx (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             False 
  ContainersReady   False 
  PodScheduled      True 
Volumes:
  default-token-8mksx:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-8mksx
    Optional:    false
QoS Class:       BestEffort
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute for 300s
                 node.kubernetes.io/unreachable:NoExecute for 300s
Events:
  Type    Reason     Age   From                                  Message
  ----    ------     ----  ----                                  -------
  Normal  Pulled     8m5s  kubelet, amitkarpe3c.mylabserver.com  Container image "fizzbuzz" already present on machine
  Normal  Created    8m4s  kubelet, amitkarpe3c.mylabserver.com  Created container
  Normal  Started    8m4s  kubelet, amitkarpe3c.mylabserver.com  Started container
  Normal  Scheduled  8m4s  default-scheduler                     Successfully assigned default/fizzbuzz to amitkarpe3c.mylabserver.com
```

- Delete all pod which have label as `run=fizzbuzz` using command `kubectl delete all -l run=fizzbuzz`
- Create pod using following command `kubectl run fizzbuzz --image=fizzbuzz --image-pull-policy=Never --restart=Never`
- If any error related to Image Pull occured, then make sure that the `imagePullPolicy` of the container to is set to `Never`. Ref: [Updating Images](https://kubernetes.io/docs/concepts/containers/images/#updating-images)
- If still image pull failed then make sure that on the same system `docker images` showing specified image.

### Output should like this


```console
cloud_user@amitkarpe3c:~$ git clone https://gitlab.com/amitkarpe/fizzbuzz.git
Cloning into 'fizzbuzz'...
remote: Enumerating objects: 35, done.
remote: Counting objects: 100% (35/35), done.
remote: Compressing objects: 100% (26/26), done.
remote: Total 35 (delta 6), reused 0 (delta 0)
Unpacking objects: 100% (35/35), done.
Checking connectivity... done.
cloud_user@amitkarpe3c:~$ cd fizzbuzz/minimal/
cloud_user@amitkarpe3c:~/fizzbuzz/minimal$ ls
Dockerfile  fizzbuzz.py  Makefile  output.txt  pod.yaml
cloud_user@amitkarpe3c:~/fizzbuzz/minimal$ make
make create-image
make[1]: Entering directory '/home/cloud_user/fizzbuzz/minimal'
echo "Make sure docker image is get create on same node where pod is going to run"
Make sure docker image is get create on same node where pod is going to run
make build
make[2]: Entering directory '/home/cloud_user/fizzbuzz/minimal'
docker build -t fizzbuzz .	
Sending build context to Docker daemon  6.656kB
Step 1/4 : FROM python:2.7-slim
 ---> f462855313cd
Step 2/4 : WORKDIR /app
 ---> Using cache
 ---> 2e140a22b0a7
Step 3/4 : COPY . /app
 ---> 04ed3c9fcec1
Step 4/4 : CMD ["python", "fizzbuzz.py"]
 ---> Running in cf20ac71b078
Removing intermediate container cf20ac71b078
 ---> b6b2201023c7
Successfully built b6b2201023c7
Successfully tagged fizzbuzz:latest
make[2]: Leaving directory '/home/cloud_user/fizzbuzz/minimal'
make check
make[2]: Entering directory '/home/cloud_user/fizzbuzz/minimal'
docker images | grep fizzbuzz
fizzbuzz                 latest              b6b2201023c7        Less than a second ago   137MB
make[2]: Leaving directory '/home/cloud_user/fizzbuzz/minimal'
make run
make[2]: Entering directory '/home/cloud_user/fizzbuzz/minimal'
docker run -it fizzbuzz
fizzbuzz
1
2
fizz
4
buzz
fizz
7
8
fizz
buzz
11
fizz
13
14
fizzbuzz
16
17
fizz
19
buzz
fizz
22
23
fizz
buzz
26
fizz
28
29
fizzbuzz
31
32
fizz
34
buzz
fizz
37
38
fizz
buzz
41
fizz
43
44
fizzbuzz
46
47
fizz
49
buzz
fizz
52
53
fizz
buzz
56
fizz
58
59
fizzbuzz
61
62
fizz
64
buzz
fizz
67
68
fizz
buzz
71
fizz
73
74
fizzbuzz
76
77
fizz
79
buzz
fizz
82
83
fizz
buzz
86
fizz
88
89
fizzbuzz
91
92
fizz
94
buzz
fizz
97
98
fizz
buzz
make[2]: Leaving directory '/home/cloud_user/fizzbuzz/minimal'
make[1]: Leaving directory '/home/cloud_user/fizzbuzz/minimal'
make create-pod
make[1]: Entering directory '/home/cloud_user/fizzbuzz/minimal'
kubectl run fizzbuzz --image=fizzbuzz --image-pull-policy=Never --restart=Never -o yaml --dry-run  > pod.yaml
make[1]: Leaving directory '/home/cloud_user/fizzbuzz/minimal'
make run-pod
make[1]: Entering directory '/home/cloud_user/fizzbuzz/minimal'
kubectl delete -f pod.yaml | echo "" | sleep 2 
Error from server (NotFound): error when deleting "pod.yaml": pods "fizzbuzz" not found
kubectl create -f pod.yaml && echo "success!" || echo "failure!"  
pod/fizzbuzz created
success!
timeout 10s kubectl get pod -w || echo ""
NAME       READY   STATUS              RESTARTS   AGE
fizzbuzz   0/1     ContainerCreating   0          0s
NAME       READY   STATUS              RESTARTS   AGE
fizzbuzz   0/1     Completed           0          0s

make[1]: Leaving directory '/home/cloud_user/fizzbuzz/minimal'
make check-logs
make[1]: Entering directory '/home/cloud_user/fizzbuzz/minimal'
kubectl logs fizzbuzz | less
make[1]: Leaving directory '/home/cloud_user/fizzbuzz/minimal'

```